
async function runsPerTeam (){

    const result = await fetch("./data/matches.csv")
   

    const matchData = await result.text();


    const matchRows = matchData.split("\n").slice(1).map((row) => {

        return row;
    })

    //console.log(matchRows)

    let match2016 = matchRows.filter((each)=>{
        let val = each.split(",");
        if(val[1]==2016)
        {
            return each;
        }
    })

    let firstId = match2016[0].split(",")[0];
    let lastId = match2016[match2016.length-1].split(",")[0];

    console.log(firstId, lastId)

        const result1 = await fetch("./data/deliveries.csv")
       
    
        const deliveriesData = await result1.text();
    
        const delRows = deliveriesData.split("\n").slice(1).map((row) => {
           
    
            return row;
        })

        // console.log(delRows)

        let filteredDel = delRows.filter((each)=>{

            let deldata = each.split(",");

            if(deldata[0]>=firstId && deldata[0]<=lastId)
            {
                return each;
            }
        })

        //console.log(filteredDel)

        const reducedData = filteredDel.reduce((result, each)=>{

            let data1 =  each.split(",");
            // console.log("Hello")
            let key = data1[3];
            if(result.hasOwnProperty(data1[3]))
            {
                result[data1[3]]+=Number(data1[16]);
            }
            else
            {
            
                result[data1[3]]=Number(data1[16]);
            }

            return result;

        },{})

        //console.log(reducedData);

        return reducedData;

}

export{ runsPerTeam }