

import { matchesWonPerTeam } from "./index2.mjs";

matchesWonPerTeam().then((data) => {
    console.log(data)

    const keys = [];
    const values = [];

    let max = 0;
    let resProp = "";
    for(let prop in data)
    {
        let len = Object.keys(data[prop]).length
        if(len>max)
        {
            resProp = prop;
            max=len;
        }
    }

    let arrX = Object.keys(data[resProp]);



    console.log(arrX)

    let teams = Object.keys(data)

    console.log(teams)

    const ctx = document.getElementById('myChart').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: arrX,
            datasets: [{
                label: teams[0],
                data: Object.values(data[teams[0]]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    
                ],
                borderColor: [
                    
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },

            {
                label: teams[1],
                data: Object.values(data[teams[1]]),
                backgroundColor: [
                    
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                   
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },

            {
                label: teams[2],
                data: Object.values(data[teams[2]]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },



            {
                label: teams[3],
                data: Object.values(data[teams[3]]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    
                ],
                borderWidth: 1
            },



            {
                label: teams[4],
                data: Object.values(data[teams[4]]),
                backgroundColor: [
                    
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                   
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },




            {
                label: teams[5],
                data: Object.values(data[teams[5]]),
                backgroundColor: [
                    
                    'rgba(75, 192, 192, 0.2)',
                   
                ],
                borderColor: [
                
                    'rgba(255, 206, 86, 1)',
                    
                ],
                borderWidth: 1
            },



            {
                label: teams[6],
                data: Object.values(data[teams[6]]),
                backgroundColor: [
                    
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },



            {
                label: teams[7],
                data: Object.values(data[teams[7]]),
                backgroundColor: [
                    
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
            
                    'rgba(255, 206, 86, 1)',
                ],
                borderWidth: 1
            },



            {
                label: teams[8],
                data: Object.values(data[teams[8]]),
                backgroundColor: [
                    
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    
                    'rgba(153, 102, 255, 1)',
                ],
                borderWidth: 1
            },



            {
                label: teams[9],
                data: Object.values(data[teams[9]]),
                backgroundColor: [
                    
                    'rgba(75, 192, 192, 0.2)',
                
                ],
                borderColor: [
                    
                    'rgba(75, 192, 192, 1)',
                    
                ],
                borderWidth: 1
            },

            {
                label: teams[10],
                data: Object.values(data[teams[10]]),
                backgroundColor: [
                    
                    'rgba(54, 162, 235, 0.2)',
                    
                ],
                borderColor: [
                    
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },

            {
                label: teams[11],
                data: Object.values(data[teams[11]]),
                backgroundColor: [
                    
                    'rgba(153, 102, 255, 0.2)',
                ],
                borderColor: [
                    
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },


            {
                label: teams[12],
                data: Object.values(data[teams[12]]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    
                ],
                borderWidth: 1
            }

            


            
        ]

            
        },


        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
})